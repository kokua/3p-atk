#!/bin/bash/env

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(dirname "$0")"

PROJECT=atk
LICENSE=README
VERSION="2.27.1"
SOURCE_DIR="$PROJECT"

 
pwd
# Save for later
#ATK_SOURCE_DIR="$PROJECT"
#VERSION="$(sed -n 's/^ *VERSION=\([0-9.]*\)$/\1/p' "../$ATK_SOURCE_DIR/configure")"

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

stage="$(pwd)"
GLIB_INCLUDE="${stage}"/packages/include/glib

[ -f "$GLIB_INCLUDE"/glib.h ] || fail "You haven't installed the glib package yet."
export PKG_CONFIG_PATH="${stage}"/packages/lib/release/pkgconfig
export LD_LIBRARY_PATH="${stage}"/packages/lib/release

# load autobuild provided shell functions and variables
source_environment_tempfile="$stage/source_environment.sh"
"$autobuild" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

build=${AUTOBUILD_BUILD_ID:=0}
echo "${VERSION}.${build}" > "${stage}/VERSION.txt"
case "$AUTOBUILD_PLATFORM" in

    "linux64")
        # Prefer gcc-4.6 if available.
        # Prefer gcc-4.9 if available.
        # Prefer gcc-6 if available.
        if [[ -x /usr/bin/gcc-6 && -x /usr/bin/g++-6 ]]; then
            export CC=/usr/bin/gcc-6
            export CXX=/usr/bin/g++-6
        fi
        # Default target to 64-bit
        opts="${TARGET_OPTS:--m64}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        # Debug first
#        pushd "$TOP/$SOURCE_DIR"
#		    CFLAGS="$opts -O0 -g -fPIC -DPIC" CXXFLAGS="$opts -O0 -g -fPIC -DPIC" \
#		    ./configure --prefix="$stage" --enable-debug --includedir="$stage/include/glib" 
#		    make
#		    make install

        # conditionally run unit tests
#        if [ "${DISABLE_UNIT_TESTS:-0}" = "0" ]; then
#            make test
#        fi
#        # clean the build artifacts
#        make distclean
#       popd 
       # Release last
        pushd "$TOP/$SOURCE_DIR"
            CFLAGS="-I$GLIB_INCLUDE $opts -O3 -fPIC -DPIC" CXXFLAGS="-I$GLIB_INCLUDE $opts -O3 -fPIC -DPIC" \
            LDFLAGS="-L$stage/packages/lib/release" \
            CC="$CC -m64" ./configure --prefix="$stage" --includedir="$stage/include/atk"
            make
            make install
		    cp -a atk/*.h "$stage/include/atk/"
        popd

        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    *)
        echo "platform not supported"
        exit -1
    ;;
esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$SOURCE_DIR/$LICENSE" "$stage/LICENSES/$PROJECT.txt"
#echo "$VERSION" > "$stage/VERSION.txt"



